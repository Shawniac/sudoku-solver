#!/usr/bin/env python
Board = list[list[int]]

def is_valid_sudoku(board: Board) -> bool:
    for row in board:
        if len(row) != 9:
            return False
        for row_entry in row:
            if not isinstance(row_entry, int) or row_entry < 0 or row_entry > 9:
                return False
    return True


def find_empty_spot(board: Board) -> tuple[int, int] | None:
    for row in range(9):
        for col in range(9):
            if board[row][col] == 0:
                return (row, col)
    return None


def valid_entry(board: Board, row: int, col: int, num: int) -> bool:
    # Check rows and columns for the number
    for i in range(9):
        if board[row][i] == num:
            return False
        if board[i][col] == num:
            return False

    # Check 3x3 sub-grid for the number
    start_row = (row // 3) * 3
    start_col = (col // 3) * 3
    for i in range(3):
        for j in range(3):
            if board[start_row + i][start_col + j] == num:
                return False

    # If none of the above conditions are met, the number is valid
    return True


def solve_sudoku(board: Board) -> bool:
    empty_spot = find_empty_spot(board)
    if empty_spot is None:
        return True
    (row, col) = empty_spot
    for num in range(1, 10):
        if valid_entry(board, row, col, num):
            board[row][col] = num
            if solve_sudoku(board):
                return True
            board[row][col] = 0
    return False


def solve_sudoku_board(board: Board) -> Board:
    if not is_valid_sudoku(board):
        raise ValueError("Not a valid sudoku board")
    if solve_sudoku(board):
        return board
    else:
        raise ValueError("Could not solve the board")


def parse_sudoku_string(sudoku_string: str) -> Board:
    board = []
    for i in range(9):
        row = []
        for j in range(9):
            if sudoku_string[i*9 + j] == '.':
                row.append(0)
            else:
                row.append(int(sudoku_string[i*9 + j]))
        board.append(row)
    return board


def print_board(board: Board) -> None:
    for i in range(9):
        if i % 3 == 0 and i != 0:
            print("- " * 11)
        for j in range(9):
            if j % 3 == 0 and j != 0:
                print("| ", end='')
            print(board[i][j], end=' ')
        print()
    print("- " * 11)
    print()


input_string_board = "..3.2.6..9..3.5..1..18.64....81.29..7.......8..67.82....26.95..8..2.3..9..5.1.3.."

input_board = parse_sudoku_string(input_string_board)
print_board(input_board)

solved_board = solve_sudoku_board(input_board)
print_board(solved_board)
